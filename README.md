5 things you need to know about CBD

You have probably been to CBD. You may have seen CBD products in stores, people are talking about it, and actually eating it without knowing it. Learn here what it's all about. What is CBD? CBD is the abbreviation for cannabidiol, a unique compound naturally found in cannabis and hemp. If you have ever smoked marijuana, you have consumed CBD, but only in very small quantities. Surely you have heard of THC - the most prominent cannabinoid responsible for the unique intoxication cannabis causes. CBD is the second most common cannabinoid in the plant, just behind the THC. Hardly anyone knew anything about CBD until a few years ago, but in recent years studies have found that this compound has a breathtaking medical value. For example, the use of CBD as an antiepileptic drug is directly responsible for the landslide change in public opinion about cannabis for medical use. With that in mind, we've listed the 5 most important things you need to know about CBD.

CBD is not "high"

 No matter how much CBD you eat, it will not make you "high". Unlike THC, CBD is not psychoactive, so you can take it without kissing the sky. If you smoked CBD-rich cannabis strains, the small amount of THC will naturally make you intoxicated, but you do not have to worry about using a pure CBD supplement.

CBD is legal in almost every country

 Thanks to the fact that CBD is not psychoactive, it is legal in most countries around the world. This means that, like any other health supplement, it may be consumed/bought and consumed without police attention.

CBD has been proven to help with a wide range of complaints

 Although not officially recognized as a drug, studies have shown that CBD is useful in a number of different diseases. These include epilepsy, anxiety disorders, sleep disorders, nausea, psychosis, depression, inflammatory, neurodegenerative diseases, and even cancer. But do not just rely on our word - start a quick search on Google and you'll find many studies looking at CBD. So promising is this substance that GW Pharmaceuticals, the company behind the world's first THC spray, is about to launch CBD as an approved pharmaceutical drug for the treatment of epilepsy. 4. Not all CBD is won in the same way The quality of products available today based on CBD can vary widely. Unfortunately, some CBD products were found that were not CBD at all or they were contaminated with pesticides and bacteria, which is contrary to the purpose of using CBD. Therefore, it is important to find a trusted supplier that sells high-quality CBD products. One such company is Cibdol, which produces 100% organically grown hemp-derived and lab-tested CBD oil. They do not use pesticides or other chemicals and laboratory tests ensure that the CBD oil always has the specified efficacy.

CBD Dampens the effects of THC

 CBD helps to attenuate and regulate the effects of THC. Have you ever experienced that the effect of the THC was "too much" for you? CBD takes the lead and is responsible for the stress-reducing effects of cannabis. Newly bred CBD-rich varieties create a much more functional intoxication, which is very useful for medical users. And that is just the beginning! Science has just begun to scratch the surface of the potential of CBD, but it is already becoming clear that CBD is a powerful supercannabinoid!
 
 https://cbdfive.net/